from django.shortcuts import render
from django.views.generic import FormView
from django.http import JsonResponse

from .forms import SignificanceForm
from .utils import calculate_p_value, p_value_to_significance, SIGNIFICANCE_THRESHOLD


class SignificanceFormView(FormView):
    template_name = 'form.html'
    form_class = SignificanceForm

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        return response

    def form_valid(self, form):
        try:
            p_value = calculate_p_value(**form.cleaned_data)
            significance = p_value_to_significance(p_value)
        except ValueError:
            p_value = 'NaN'
            significance = p_value_to_significance(SIGNIFICANCE_THRESHOLD + 1)

        result = {
            'p_value': p_value,
            'significance': significance,
        }

        if self.request.is_ajax():
            return JsonResponse(result)

        data = self.get_context_data()
        data.update(result)

        return render(self.request, self.template_name, data)
