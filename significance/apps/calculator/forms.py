from django import forms


class SignificanceForm(forms.Form):
    control_visitors = forms.IntegerField(min_value=15)
    variation_visitors = forms.IntegerField(min_value=15)
    control_conversions = forms.IntegerField()
    variation_conversions = forms.IntegerField()
