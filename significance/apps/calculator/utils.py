from math import sqrt


class ControlVisitorError(Exception):
    pass


class VariationVisitorError(Exception):
    pass


SIGNIFICANCE_THRESHOLD = 0.05


# taken from https://vwo.com/ab-split-test-significance-calculator/
def normal_p(x):
    d1 = 0.0498673470
    d2 = 0.0211410061
    d3 = 0.0032776263
    d4 = 0.0000380036
    d5 = 0.0000488906
    d6 = 0.0000053830
    a = abs(x)
    t = 1.0 + a * (d1 + a * (d2 + a * (d3 + a * (d4 + a * (d5 + a * d6)))))
    t *= t
    t *= t
    t *= t
    t *= t
    t = 1.0 / (t + t)
    if (x >= 0):
        t = 1 - t
    return t

# contracts:
# control_visitors >= 15
# variation_visitors >= 15
def calculate_p_value(control_visitors,
                      variation_visitors,
                      control_conversions,
                      variation_conversions,
                      round_digits=3):
    '''
    >>> calculate_p_value(control_visitors=90,
    ...                   variation_visitors=20,
    ...                   control_conversions=9,
    ...                   variation_conversions=9)
    0.001
    >>> calculate_p_value(control_visitors=10,
    ...                   variation_visitors=20,
    ...                   control_conversions=9,
    ...                   variation_conversions=9)
    Traceback (most recent call last):
    ControlVisitorError
    '''
    if control_visitors < 15:
        raise ControlVisitorError

    if variation_visitors < 15:
        raise VariationVisitorError

    control_p = control_conversions / control_visitors
    variation_p = variation_conversions / variation_visitors

    std_error = sqrt(
        (control_p * (1 - control_p) / control_visitors) +
        (variation_p * (1 - variation_p) / variation_visitors)
    )

    z_value = (variation_p - control_p) / std_error
    p_value = normal_p(z_value)

    if (p_value > 0.5):
        p_value = 1 - p_value

    return round(p_value, round_digits)


def p_value_to_significance(p):
    if p < SIGNIFICANCE_THRESHOLD:
        return 'Yes'
    return 'No'


if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.ELLIPSIS)
