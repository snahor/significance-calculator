from django.conf.urls import url
from .views import SignificanceFormView


urlpatterns = (
    url(r'^$', SignificanceFormView.as_view()),
)
